﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : CharactersController {

    [SerializeField]
    PlayerInfo playerInfo;

    [SerializeField]
    private GameObject weaponPlaceHolder;

    [SerializeField]
    private static Vector3 posCamaraOriginal;

    [SerializeField]
    private float rangeActionValue;

    [SerializeField]
    private bool pointing, changingWeapon;

    //[SerializeField]
    //private int numMaxGranades = 1;


    private int actualLife, initialLife;

    //Camera Movement
    public float xSensitivity = 100.0f, verticalVelocity;
    public float ySensitivity = 100.0f;


    float mouseX, mouseY;

    private CharacterController characterController;

    private Camera cameraPlayer;
    private CameraController cameraController;


    private WeaponController activeWeapon;
    private WeaponController weaponBeforeGranade;

    public WeaponController ActiveWeapon { get => activeWeapon; set => activeWeapon = value; }

    public int ActualLife { get => actualLife; set => actualLife = value; }
    public int InitialLife { get => initialLife; set => initialLife = value; }

    //On Level Controller
    public static event Action<WeaponController> OnChangeWeapon = delegate { };
    public static event Action OnReloadWeapon = delegate { };

    public static event Action<WeaponController> OnPlayerShoot = delegate { };
    public static event Action<Vector3> OnPlayerMakeSound = delegate { };


    public static event Action<DoorController> OnFrontOfDoor = delegate { };
    public static event Action OnPlayerDead = delegate { };
    public static event Action OnHideActionText = delegate { };
    public static event Action OnOpenInventary = delegate { };

    public static event Action<int> OnPlayerHit = delegate { };
    public static event Action<int> OnPlayerHeal = delegate { };
    public static event Action<DoorController> OnPlayerIntereactWithDoor = delegate { };

    IPlayerInput playerInput;

    private bool openInventary;
    // Start is called before the first frame update
    void Start ()
    {
        characterController = GetComponent<CharacterController>();
        cameraPlayer = GetComponentInChildren<Camera>();
        cameraController = cameraPlayer.GetComponent<CameraController>();
        posCamaraOriginal = cameraPlayer.transform.localPosition;
        ActiveWeapon = WeaponPool.GetWeaponController(0);

        OnChangeWeapon(ActiveWeapon);
        //Refactorizar
        ActiveWeapon.Activate(true);
        ActualLife = InitialLife;
        TimerRifleInstance = Instantiate(TimerPrefab);
        TimerRifle = TimerRifleInstance.GetComponent<Timer>();

        TimerRifle.OnTimeDone += CanShootRifle;
        playerInput = new KeyboardController();
    }

    // Update is called once per frame
    void Update()
    {
        CheckInventary();
        if (!openInventary)
        {
            CameraMovement();
            PlayerMovement();
            CheckDoors();
            if (!changingWeapon)
            {
                Pointing();
                Shoot();
            }

            if (!pointing)
            {
                SelectNewWeapon();
            }
            if (!changingWeapon && !pointing)
            {
                ReloadWeapon();
            }
        }
    }

    private void CheckInventary()
    {

        if (playerInput.OpenInventory())
        {
            openInventary = !openInventary;
            OnOpenInventary();
        }
    }

    private void CheckDoors()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, rangeActionValue))
        {
            DoorController door = hit.collider.GetComponentInParent<DoorController>();
            if (door)
            {
                OnFrontOfDoor(door);
                //TO DO Refactorizar
                if (playerInput.Action())
                {
                    OnPlayerIntereactWithDoor(door);
                }
            }
        }
        else
        {
            OnHideActionText();
        }
    }

    private void ReloadWeapon()
    {
        if (playerInput.Reload())
        {
            Debug.Log("Reload");
            if (ActiveWeapon.GetType().Equals(typeof(RifleBase)))
            {
                RifleBase rifle = (RifleBase)ActiveWeapon;
                rifle.Reload();
                OnReloadWeapon();
            }
            if (ActiveWeapon.GetType().Equals(typeof(GunController)))
            {
                GunController gun = (GunController)ActiveWeapon;
                gun.Reload();
                OnReloadWeapon();
            }
        }
    }

    private void ShootRifle()
    {
        RifleBase fusil = (RifleBase)ActiveWeapon;
        TimerRifle.WaitTime = fusil.GetCadenceBullets();
        if (fusil.NumBullets > 0 && CanShotRifle)
        {
            fusil.Shoot();
            CanShotRifle = false;
            OnPlayerShoot?.Invoke(fusil);
            TimerRifle.gameObject.SetActive(true);
            TimerRifle.Reset();
        }
    }

    private void ShootRifleOneTime()
    {
        RiflePlayerController fusil = (RiflePlayerController)ActiveWeapon;
        if (fusil.NumBullets > 0)
        {
            fusil.Shoot();
            OnPlayerShoot?.Invoke(fusil);
        }
    }

    private void Shoot()
    {

        //Mientras lo pulsamos
        if (playerInput.ShootHoldOn())
        //if (Input.GetButton("Fire1"))
        {
            if (ActiveWeapon.GetComponent<RiflePlayerController>())
            {
                if (ActiveWeapon.GetComponent<RiflePlayerController>().QuickShoot)
                {
                    Debug.Log("Disparamos fusil n vez");
                    ShootRifle();
                    OnPlayerMakeSound(this.transform.position);
                }
            }
        }

        //Solo 1 vez
        if (playerInput.ShootOnce())

        //if (Input.GetButtonDown("Fire1"))
        {
            if (ActiveWeapon.GetType().Equals(typeof(GunController)))
            {
                //Debug.Log("Disparamos pistola 1 vez");
                GunController gun = (GunController)ActiveWeapon;
                TimerRifle.WaitTime = gun.GetCadenceBullets();
                if (gun.GetUsableBullets() > 0)
                {
                    gun.Shoot();
                    OnPlayerShoot?.Invoke(gun);
                    OnPlayerMakeSound(this.transform.position);

                }
            }
            if (ActiveWeapon.GetType().Equals(typeof(OneHandWeaponController)))
            {
                Debug.Log("Golpeamos con barra");
                OneHandWeaponController oneHandWeapon = (OneHandWeaponController)ActiveWeapon;
                oneHandWeapon.DoHit();
            }
            if (playerInput.ShootHoldOn())
            {
                if (ActiveWeapon.GetComponent<RiflePlayerController>())
                {
                    if (!ActiveWeapon.GetComponent<RiflePlayerController>().QuickShoot)
                    {
                        Debug.Log("Disparamos fusil lento 1 vez");
                        ShootRifleOneTime();
                        OnPlayerMakeSound(this.transform.position);
                    }
                }
            }

            if (ActiveWeapon.GetType().Equals(typeof(GranadeController)))
            {
                if (playerInfo.numMaxGranades > 0)
                {
                    GranadeController granade = (GranadeController)ActiveWeapon;
                    granade.ThrowGranade();
                    ChangeWeapon(weaponBeforeGranade);
                }
            }
        }
    }



    //TODO Refactorizar
    private void SelectNewWeapon()
    {
        WeaponController newWeapon = null;

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            newWeapon = WeaponPool.GetWeaponController(0);
            if (newWeapon)
            {
                if (ActiveWeapon != newWeapon)
                {
                    ChangeWeapon(newWeapon);
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            newWeapon = WeaponPool.GetWeaponController(1);
            if (newWeapon)
            {
                if (ActiveWeapon != newWeapon)
                {
                    ChangeWeapon(newWeapon);
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            newWeapon = WeaponPool.GetWeaponController(2);
            if (newWeapon)
            {
                if (ActiveWeapon != newWeapon)
                {
                    ChangeWeapon(newWeapon);
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            newWeapon = WeaponPool.GetWeaponController(3);
            if (newWeapon)
            {
                if (ActiveWeapon != newWeapon)
                {
                    ChangeWeapon(newWeapon);
                }
            }
        }
        if (playerInput.SelectGranade())
        {
            newWeapon = WeaponPool.GetGranadeController();
            if (newWeapon)
            {
                if (ActiveWeapon != newWeapon)
                {
                    weaponBeforeGranade = ActiveWeapon;
                    ChangeWeapon(newWeapon);
                }
            }
        }
        if (playerInput.GetHandWeapon())
        {
            newWeapon = WeaponPool.GetOneHandWeaponController(WeaponPool.LastOneHandWeaponSelected);
            if (newWeapon)
            {
                if (ActiveWeapon != newWeapon)
                {
                    ChangeWeapon(newWeapon);
                }
            }
        }

        if (ActiveWeapon.GetType().Equals(typeof(OneHandWeaponController)))
        {
            float changeValue = playerInput.ChangeValueSelectedOneHandWeapon();

            if (changeValue < 0)
            {
                WeaponPool.LastOneHandWeaponSelected--;
            }
            else if(changeValue > 0)
            {
                WeaponPool.LastOneHandWeaponSelected++;
            }
            newWeapon = WeaponPool.GetOneHandWeaponController(WeaponPool.LastOneHandWeaponSelected);
            ChangeWeapon(newWeapon);


        }

        newWeapon = null;
    }

    private void ChangeWeapon(WeaponController newWeapon)
    {
        changingWeapon = true;

        if (ActiveWeapon.GetType().Equals(typeof(GranadeController)))
        {
            GranadeController granade = (GranadeController)ActiveWeapon;
            if (granade.Thrown)
            {
                newWeapon.MoveAway();
                newWeapon.Activate(true);
                ActiveWeapon = newWeapon;

                changingWeapon = false;
                return;
            }
        }
        else
        {

            newWeapon.MoveAway();

            newWeapon.Activate(true);
            ActiveWeapon.Activate(false);
            OnChangeWeapon?.Invoke(newWeapon);
            ActiveWeapon = newWeapon;

            changingWeapon = false;
        }
    }

    private void Pointing()
    {
        //To Check positions
        //camera.transform.localPosition = Vector3.Lerp(camera.transform.localPosition, activeWeapon.PointPosition.localPosition, 0.2f);

        if (playerInput.Point())
        {
            pointing = true;
            ActiveWeapon.MoveToOrigin();
            if (ActiveWeapon.GetType().Equals(typeof(RiflePlayerController)))
            {
                RiflePlayerController fusil = (RiflePlayerController)ActiveWeapon;

                cameraController.LerpPosition(fusil.PointPosition.localPosition, 0.2f);

            }
            if (ActiveWeapon.GetType().Equals(typeof(IGunController)))
            {
                GunController gun = (GunController)ActiveWeapon;

                cameraController.LerpPosition(gun.PointPosition.localPosition, 0.2f);

            }

            if (ActiveWeapon.GetType().Equals(typeof(GranadeController)))
            {
                GranadeController granade = (GranadeController)ActiveWeapon;
                //Show X in canvas

            }
        }
        else
        {
            pointing = false;
            cameraController.LerpPosition(posCamaraOriginal, 0.2f);
        }
    }

    internal void AddDamage(int damage)
    {
        if (ActualLife - damage >= 0)
        {
            ActualLife -= damage;
            //Debug.Log("LIFE " + ActualLife);
            OnPlayerHit(damage);

            if (ActualLife == 0)
            {
                OnPlayerDead();
            }
        }
    }

    internal void HealDamage(int heal)
    {
        if (ActualLife + heal <= InitialLife)
        {
            ActualLife += heal;
            Debug.Log("LIFE " + ActualLife);
            OnPlayerHeal(heal);
        }
        else
        {

        }
    }
    private void PlayerMovement()
    {
        if (characterController.isGrounded)
        {
            verticalVelocity = -GravityForce * Time.deltaTime;
            if (playerInput.Jump())
            {
                verticalVelocity = PotenciaSalto;
            }
        }
        else
        {
            verticalVelocity -= GravityForce * Time.deltaTime;
        }


        Vector3 vectorHorizontal = transform.right * playerInput.MovePlayerHorizontal();
        Vector3 vectorVertical = transform.forward * playerInput.MovePlayerVertical();

        Vector3 vectorSalto = new Vector3(0, verticalVelocity, 0);
        Vector3 vectorMov = (vectorHorizontal + vectorVertical) * VelocidadCorrer * Speed + vectorSalto;
        characterController.Move(vectorMov * Time.deltaTime);


        if ((playerInput.MovePlayerHorizontal() != 0 || playerInput.MovePlayerVertical() != 0) && !pointing)
        {
            ActiveWeapon.MoveWeapon();
        }
    }


    private void CameraMovement()
    {
        mouseX += playerInput.MoveCameraX() * Time.deltaTime * xSensitivity;
        mouseY += playerInput.MoveCameraY() * Time.deltaTime * ySensitivity;


        mouseY = Mathf.Clamp(mouseY, -45.0f, 45.0f);
        characterController.transform.localEulerAngles = new Vector3(-mouseY, mouseX, 0);
    }

    private void OnTriggerEnter(Collider other)
    {
        BulletController bulletController = other.GetComponent<BulletController>();

        if (bulletController)
        {
            OnPlayerHit(bulletController.Damage);
            bulletController.gameObject.SetActive(false);
            Debug.Log("Player Hit");
        }
    }

    private void OnDrawGizmos()
    {

        Gizmos.color = Color.blue;
        Vector3 direction = transform.TransformDirection(Vector3.forward) * rangeActionValue;
        Gizmos.DrawRay(transform.position, direction);

    }
}

