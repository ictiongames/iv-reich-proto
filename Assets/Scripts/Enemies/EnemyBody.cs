﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyBody : MonoBehaviour {

    [SerializeField]
    private Transform[] patrolPoints;
    [SerializeField]
    private DoorController lastDoorOpened;
    private NavMeshAgent navMeshAgent;
    private EnemyController enemyController;
    private int patrolPointToGo = 0;
    private bool _moveAcrossNavMeshesStarted, canOpenDoor;
    private DoorController doorController;
    private float distanceToCheckThings = 5.0f;
    public Transform[] PatrolPoints { get => patrolPoints; set => patrolPoints = value; }
    public bool GoToDoor { get => _goToDoor; set => _goToDoor = value; }
    public DoorController DoorController { get => doorController; set => doorController = value; }

    public event Action OnOffLinkStart = delegate { };
    public event Action OnOffLinkEnd = delegate { };
    public event Action OnCantOpenDoor = delegate { };

    private bool _goToDoor, _passingDoor;
    private Vector3 doorEndPos;

    private Vector3[] pathVectors;
    private bool showGizmos;
    private Animator animator;


    // Start is called before the first frame update
    void Start()
    {
        enemyController = GetComponent<EnemyController>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        navMeshAgent.speed = enemyController.Speed;
        animator = GetComponent<Animator>();
    }


    internal void SetDestination(Vector3 positionToCheck)
    {
        animator.SetBool("Walk", true);
        if (positionToCheck != null)
        {
            navMeshAgent.speed = enemyController.Speed;
            //navMeshAgent.SetDestination(hit.position);


            navMeshAgent.SetDestination(positionToCheck);
            Vector3 dir = positionToCheck - transform.position;
            dir.y = 0; // keep the direction strictly horizontal
            Quaternion rot = Quaternion.LookRotation(dir);
            // slerp to the desired rotation over time
            transform.rotation = Quaternion.Slerp(transform.rotation, rot, 1.5f * Time.deltaTime);
        }
    }


    internal void ChangeLookAt(Transform transform)
    {
        //vec
        transform.LookAt(transform);
    }

    internal void StopMovement()
    {
        navMeshAgent.speed = 0;
        animator.SetBool("Walk", false);
    }

    internal void CheckPatrolPoints(Transform patrolPointReached)
    {
        patrolPointToGo++;
        if (patrolPointToGo >= PatrolPoints.Length)
        {
            patrolPointToGo = 0;
        }
        navMeshAgent.SetDestination(PatrolPoints[patrolPointToGo].position);
    }

    internal void StartPatrol()
    {
        transform.position = PatrolPoints[0].position;
    }

    internal bool IsAwayFromPatrolPoint()
    {
        bool backToPatrol = false;
        if (Vector3.Distance(transform.position, PatrolPoints[patrolPointToGo].position) > enemyController.EnemyInfo.distanceToFollowPlayer)
        {
            backToPatrol = true;
        }
        return backToPatrol;
    }

    internal void ContinuePatrol()
    {
        MoveToPosition(PatrolPoints[patrolPointToGo].position);
    }


    //IEnumerator MoveAcrossNavMeshLink()
    //{
    //    //TO DO
    //    //Get rotacion para que vaya siempre al contrario que el forward de la puerta
    //    transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.AngleAxis(-90.0f, Vector3.up), 0.1f);
    //    Vector3 startPos = navMeshAgent.transform.position;
    //    float initialY = transform.position.y;
    //    Vector3 endPos = doorEndPos + Vector3.up * navMeshAgent.baseOffset;
    //    endPos = new Vector3(endPos.x, initialY, endPos.z);

    //    float duration = (endPos - startPos).magnitude / navMeshAgent.velocity.magnitude;
    //    float t = 0.0f;
    //    float tStep = 1.0f / duration;
    //    while (t < 1.0f)
    //    {
    //        transform.position = Vector3.Lerp(startPos, endPos, t);
    //        transform.position = new Vector3(transform.position.x, initialY, transform.position.z);

    //        //Debug.Log("Position " + transform.position);
    //        navMeshAgent.destination = transform.position;
    //        t += tStep * Time.deltaTime;
    //        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.AngleAxis(-90.0f, Vector3.up), 0.1f);
    //        yield return null;

    //    }
    //    transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.AngleAxis(-90.0f, Vector3.up), 0.1f);

    //    transform.position = endPos;
    //    //transform.position = new Vector3(transform.position.x, initialY, transform.position.z);
    //    //navMeshAgent.updateRotation = true;
    //    navMeshAgent.CompleteOffMeshLink();
    //    _moveAcrossNavMeshesStarted = false;
    //    OnOffLinkEnd();
    //    GoToDoor = false;
    //    _passingDoor = false;
    //    Invoke("ClearLastDoor", 4.0f);
    //}


    private void Update()
    {


    }

    private void OnTriggerEnter(Collider other)
    {
        OneHandWeaponController oneHandWeaponController = other.GetComponent<OneHandWeaponController>();
        DoorController doorController = other.GetComponent<DoorController>();
        DoorController doorControllerTriggers = other.GetComponentInParent<DoorController>();
        if (oneHandWeaponController)
        {
            if (oneHandWeaponController.Hit)
            {
                Debug.Log("Hammer");
            }
        }

        if (other.gameObject.tag == "PatrolPoint")
        {
            CheckPatrolPoints(other.gameObject.transform);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Vector3 direction = transform.TransformDirection(Vector3.forward) * distanceToCheckThings;
        Gizmos.DrawRay(transform.position, direction);

        if (showGizmos)
        {
            Gizmos.color = Color.green;
            for (int i = 0; i < pathVectors.Length - 1; i++)
            {
                Gizmos.DrawRay(pathVectors[i], pathVectors[i + 1] - pathVectors[i]);
            }
        }
    }

    internal void StopWalking()
    {
        animator.SetBool("Walk", false);
    }

    //internal void SetDootToPass(DoorController door)
    //{
    //    DoorController = door;
    //    //GoToDoor = true;
    //}


    internal void MoveToPosition(Vector3 positionToCheck)
    {

        List<GameObject> obstacles = ObstaclesToDestination(positionToCheck);
        int obstacleCount = obstacles.FindAll(x => !x.GetComponent<PlayerController>()).Count;
        if (obstacleCount == 0)
        {
            Debug.Log("No hay obstaculos");
            SetDestination(positionToCheck);
        }
        else
        {
            if (obstacles.Exists(x => x.GetComponentInParent<DoorController>()))
            {
                DoorController door = obstacles.Find(x => x.GetComponentInParent<DoorController>()).GetComponentInParent<DoorController>();
                Debug.Log("Hay Puerta");

                if (door)
                {
                    if (door.CanOpenDoor(this) || door.Open)
                    {
                        SetDestination(positionToCheck);
                    }
                    else
                    {
                        OnCantOpenDoor();
                    }
                }
                else
                {
                    SetDestination(positionToCheck);
                }
            }
        }
    }

    private Vector3 GetNearPartDoor(DoorController door)
    {
        if (Vector3.Distance(transform.position, door.FrontCollider.transform.position) < Vector3.Distance(transform.position, door.BackCollider.transform.position))
        {
            return door.FrontCollider.transform.position;
        }
        else
        {
            return door.BackCollider.transform.position;
        }
    }

    private List<GameObject> ObstaclesToDestination(Vector3 positionToCheck)
    {
        NavMeshPath path = new NavMeshPath();
        RaycastHit hit;
        List<GameObject> obstacles = new List<GameObject>();
        if (navMeshAgent.CalculatePath(positionToCheck, path))
        {
            pathVectors = new Vector3[path.corners.Length + 2];
            pathVectors[0] = transform.position;
            for (int i = 0; i < path.corners.Length; i++)
            {
                pathVectors[i + 1] = path.corners[i];
            }
            pathVectors[pathVectors.Length - 1] = positionToCheck;
            showGizmos = true;
            for (int i = 0; i < pathVectors.Length - 1; i++)
            {
                if (Physics.Raycast(pathVectors[i], pathVectors[i + 1] - pathVectors[i], out hit, Vector3.Distance(pathVectors[i + 1], pathVectors[i])))
                {
                    if (hit.collider.gameObject.GetComponentInChildren<MeshRenderer>())
                    {
                        obstacles.Add(hit.collider.gameObject);

                    }
                }
            }
        }
        return obstacles;
    }
}
