﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class Enemy {
    private readonly float health;
    public Enemy(EnemyBaseInfo enemy)
    {

    }
}

public class EnemyController : CharactersController {
    // Start is called before the first frame update
    public event Action<float> OnReceiveDamage = delegate { };
    //public event Action<Vector3> OnHearSound = delegate { };
    //public event Action<Transform> OnPatrolPointReached = delegate { };


    private int numMaxGranades = 1;
    [SerializeField]
    private readonly float health;

    [SerializeField]
    private GameObject activeWeaponPrefab;
    [SerializeField]
    private Transform weaponPosition;

    private bool _moveAcrossNavMeshesStarted;
    [SerializeField]
    private EnemyBaseInfo enemyInfo;

    private WeaponController activeWeapon;

    public WeaponController ActiveWeapon { get => activeWeapon; set => activeWeapon = value; }

    public EnemyBaseInfo EnemyInfo { get => enemyInfo; set => enemyInfo = value; }

    public Enemy enemy;
    void Awake()
    {
        enemy = new Enemy(EnemyInfo);
        //health = EnemyInfo.health;
        //distanceToShoot = EnemyInfo.distanceToShoot;
        //hearRadius = EnemyInfo.distanceToShoot;
        //timeToAlarm = EnemyInfo.distanceToShoot;
        //lifeToRun = EnemyInfo.distanceToShoot;
        numMaxGranades = EnemyInfo.numMaxGranades;

        //activeWeaponInstance = Instantiate(activeWeaponPrefab, weaponPosition);
        ActiveWeapon = activeWeaponPrefab.GetComponent<WeaponController>();
        //WeaponPool = GetComponentInChildren<WeaponPool>();
        //ActiveWeapon = WeaponPool.GetWeaponController(0);
        ////OnChangeWeapon(activeWeapon);

        activeWeapon.ShowGizmos(true);

        TimerRifleInstance = Instantiate(TimerPrefab);
        TimerGunInstance = Instantiate(TimerPrefab);

        TimerRifle = TimerRifleInstance.GetComponent<Timer>();
        TimerGun = TimerGunInstance.GetComponent<Timer>();

        TimerRifle.OnTimeDone += CanShootRifle;
        TimerGun.OnTimeDone += CanShootGun;
    }

    internal void ShootGun(PlayerController target)
    {
        GunController gun = ActiveWeapon.GetComponent<GunController>();
        gun.Target = target;
        TimerGun.WaitTime = gun.GetCadenceBullets();
        if (CanShotGun)
        {
            gun.Shoot();
            CanShotRifle = false;
            TimerGun.gameObject.SetActive(true);
            TimerGun.Reset();
        }
    }


    internal void ShootRifle(PlayerController target)
    {
        RifleEnemyController fusil = ActiveWeapon.GetComponent<RifleEnemyController>();
        //fusil.Target = target;
        TimerRifle.WaitTime = fusil.GetCadenceBullets();
        if (CanShotRifle)
        {
            fusil.Shoot();
            CanShotRifle = false;
            TimerRifle.gameObject.SetActive(true);
            TimerRifle.Reset();
        }
    }


    internal void ReceiveDamage(float damage)
    {
        OnReceiveDamage(damage);
    }


    private void OnCollisionEnter(Collision collider)
    {
        Debug.Log("F");
    }

}
