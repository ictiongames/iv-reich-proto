﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum EnemyStatus { Idle, Following, Shooting, Dead, RunAway, Patrol, Investigate, BackToPatrolPoint, SearchingPlayer, DoorPassing };

public class EnemyMind : MonoBehaviour {
    [SerializeField]
    private EnemyStatus status;
    [SerializeField]
    private GameObject timerPrefab;

    private GameObject timerEyeSightInstance;

    private EnemySight enemySight;
    private Timer timerEyeSight;

    //TODO Obtener mediante DI
    [SerializeField]
    private PlayerController target, playerReference;

    [SerializeField]
    private float distanceToStopSearching = 15.0f;

    private EnemyBody enemyBody;
    private EnemyController enemy;
    private NavMeshAgent navMeshAgent;

    private Vector3 _lastPlayerPositionHeared;

    public EnemyStatus Status { get => status; set => status = value; }
    public PlayerController Target { get => target; set => target = value; }
    public PlayerController PlayerReference { get => playerReference; set => playerReference = value; }

    private Vector3[] pathVectors;
    private bool showGizmos;

    void Awake()
    {
        Status = EnemyStatus.Idle;
        timerEyeSightInstance = Instantiate(timerPrefab);
        timerEyeSight = timerEyeSightInstance.GetComponent<Timer>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        enemy = GetComponent<EnemyController>();
        enemySight = GetComponent<EnemySight>();
        enemyBody = GetComponent<EnemyBody>();
        enemySight.PlayerReference = PlayerReference;
        enemy.OnReceiveDamage += OnReceiveDamage;
        //enemy.OnHearSound += CheckSoundAt;
        //enemy.OnPatrolPointReached += OnSetPatrolStatus;
        enemyBody.OnOffLinkEnd += OnOffLinkEnd;
        enemyBody.OnOffLinkStart += OnOffLinkStart;
        enemyBody.OnCantOpenDoor += OnCantOpenDoor;
        //timerEyeSight.OnTimeDone += FollowPlayer;
        ResetPlayerValues();
    }


    private void OnCantOpenDoor()
    {
        status = EnemyStatus.BackToPatrolPoint;

    }
    private void OnOffLinkStart()
    {
        Debug.Log("empieza OffLink");
        status = EnemyStatus.DoorPassing;
    }

    private void OnOffLinkEnd()
    {
        Debug.Log("End OffLink");
        if (_lastPlayerPositionHeared != Vector3.zero || enemySight.LastPlayerPosition != Vector3.zero)
        {
            status = EnemyStatus.Investigate;
        }
        else
        {
            status = EnemyStatus.Patrol;
        }
    }

    private void OnSetPatrolStatus(Transform obj)
    {
        if (status == EnemyStatus.Patrol || status == EnemyStatus.BackToPatrolPoint)
        {
            status = EnemyStatus.Patrol;
            enemyBody.ContinuePatrol();
        }
    }

    private void OnReceiveDamage(float damage)
    {
        //TODO Terminar
        //enemy.enemy.Health -= damage;
        //if (enemy.enemy.health <= 0)
        //{
        //    Status = EnemyStatus.Dead;
        //}
        //else if (enemy.Health == enemy.EnemyInfo.lifeToRun)
        //{
        //    Status = EnemyStatus.RunAway;
        //}
    }

    // Update is called once per frame
    void Update()
    {
        if (Status != EnemyStatus.Dead)
        {
            if (Status != EnemyStatus.RunAway)
            {
                if (status == EnemyStatus.Idle)
                {
                    DoPatrol();
                }
                else
                if (Status == EnemyStatus.Patrol)
                {
                    //Debug.Log("Doing Patrol");
                    CheckEyeSight();
                    //CheckObstaclesToPosition();
                    enemyBody.ContinuePatrol();
                }
                else
                if (status == EnemyStatus.BackToPatrolPoint)
                {
                    Debug.Log("Back To Patrol Patrol");
                    status = EnemyStatus.Patrol;
                    ResetPlayerValues();
                    //enemyBody.ContinuePatrol();
                }
                else
                if (status == EnemyStatus.Investigate)
                {
                    Debug.Log("Investigate something");
                    Investigate();
                }
                else if (Target != null)
                {
                    CheckDistanceToPlayer();
                    if (Status == EnemyStatus.Following)
                    {
                        if (enemySight.CanSeePlayerNear())
                        {
                            //Debug.Log("Follow player");
                            FollowPlayer();
                        }
                        else
                        {
                            Debug.Log("Player perdido tras seguirlo, buscando ultima posicion");
                            SetPlayerLastPosition();
                        }
                    }
                    if (Status == EnemyStatus.Shooting)
                    {
                        Debug.Log("Shooting");
                        if (enemySight.CanSeePlayerNear() && !enemySight.CheckIfObjectsBetweenPlayerAndEnemy())
                        {
                            StopWalking();
                            ShootTarget();
                        }
                        else
                        {
                            Debug.Log("Player perdido tras disparar, buscando ultima posicion");
                            SetPlayerLastPosition();
                        }
                    }
                    if (Status == EnemyStatus.SearchingPlayer)
                    {
                        Debug.Log("Searching Player");
                        //SearchPlayerAtLastPlaceSaw();
                        SearchPlayerAtPosition(enemySight.LastPlayerPosition);
                    }
                }
            }
        }
        else
        {
            EnemyDead();
        }
    }

    private void StopWalking() {
        enemyBody.StopMovement();
        
    }

    private void Investigate()
    {
        CheckEyeSight();

        CheckDistanteToPatrolPoints();
        if (status != EnemyStatus.BackToPatrolPoint)
        {
            if (enemySight.LastPlayerPosition != Vector3.zero)
            {
                Debug.Log("Investigate last saw");
                SearchPlayerAtPosition(enemySight.LastPlayerPosition);
            }
            else if (_lastPlayerPositionHeared != Vector3.zero)
            {
                Debug.Log("Investigate last heard");

                SearchPlayerAtPosition(_lastPlayerPositionHeared);
            }
        }
    }

    private void ResetPlayerValues()
    {
        Target = null;
        _lastPlayerPositionHeared = Vector3.zero;
        enemySight.LastPlayerPosition = Vector3.zero;
    }

    private void SetPlayerLastPosition()
    {
        enemySight.LastPlayerPosition = playerReference.transform.position;
        Status = EnemyStatus.SearchingPlayer;
    }

    private void CheckDistanteToPatrolPoints()
    {
        if (enemyBody.IsAwayFromPatrolPoint())
        {
            Debug.Log("Enemy lejos de punto de patrulla");
            status = EnemyStatus.BackToPatrolPoint;
        }
    }

    private void DoPatrol()
    {
        enemyBody.StartPatrol();
        Status = EnemyStatus.Patrol;
    }

    private void EnemyDead()
    {
        enemyBody.StopMovement();
    }

    public void FollowPlayer()
    {
        GoToDestination(Target);
    }

    private void CheckEyeSight()
    {
        GameObject objectSaw = enemySight.CheckIfSomeObjectEnterFieldOfView();
        GameObject objectSawBetweenPlayerAndEnemy = enemySight.CheckIfObjectsBetweenPlayerAndEnemy();
        //DoorController doorController = objectSawBetweenPlayerAndEnemy.GetComponent<DoorController>();
        if (!objectSawBetweenPlayerAndEnemy)
        {
            if (enemySight.CanSeePlayerFar())
            {
                Debug.Log("Player Visto lejos");
                status = EnemyStatus.Investigate;

                enemySight.LastPlayerPosition = PlayerReference.transform.position;
            }
            else if (enemySight.CanSeePlayerNear())
            {
                Debug.Log("Player Visto cerca");
                Target = PlayerReference;

                Status = EnemyStatus.Following;
            }
        }
    }


    private void SearchPlayerAtPosition(Vector3 positionToCheck)
    {
        //If we are close the position and we dont see player then go back to patrol

        enemyBody.MoveToPosition(positionToCheck);

        Debug.Log("Buscando en ultima posicion donde vimos u oimos al player");
        if (Vector3.Distance(enemyBody.transform.position, positionToCheck) < distanceToStopSearching)
        {
            Debug.Log("Llegamos a ultima posicion");
            Debug.Log("Hemos llegado cerca de la ultima posicion del player");
            CheckEyeSight();
            //No hemos visto u oido al player (o lo que hubiera ido a mirar) al llegar a su ultima posicion
            //Al no verlo reseteamos
            if (status == EnemyStatus.Investigate || status == EnemyStatus.SearchingPlayer)
            {
                Debug.Log("Player no estaba en ultima posicion, vuelvo a patrullar");
                status = EnemyStatus.BackToPatrolPoint;
            }
        }
    }


    private void CheckDistanceToPlayer()
    {
        float distance = Math.Abs(Vector3.Distance(Target.transform.position, transform.position));
        //Comprobamos si hay algun objeto entre el player y el enemigo, si hay entonces vamos a la ultima posicion, si no disparamos o le seguimos
        if (!enemySight.CheckIfObjectsBetweenPlayerAndEnemy())
        {
            if (distance < enemy.EnemyInfo.distanceToShoot)
            {
                Status = EnemyStatus.Shooting;
            }
            //If distance > 10, seguir
            else if (distance > enemy.EnemyInfo.distanceToShoot && distance < enemy.EnemyInfo.maxEyeSightDistance)
            {
                Status = EnemyStatus.Following;
            }
        }
    }


    internal void CheckSoundAt(Vector3 positionToCheck)
    {
        float distanceToSound = CalculatePathLengthToPosition(positionToCheck);
        if (distanceToSound > 0 && distanceToSound < enemy.EnemyInfo.hearRadius)
        {
            _lastPlayerPositionHeared = positionToCheck;
            status = EnemyStatus.Investigate;
        }
    }

    private float CalculatePathLengthToPosition(Vector3 playerPosition)
    {
        NavMeshPath navMeshPath = new NavMeshPath();
        float pathLength = 0;
        if (navMeshAgent)
        {
            navMeshAgent.CalculatePath(playerPosition, navMeshPath);
            if (navMeshPath.status != NavMeshPathStatus.PathInvalid)
            {
                Vector3[] pointsOfPath = new Vector3[navMeshPath.corners.Length + 2]; //Including player and enemy
                pointsOfPath[0] = transform.position;
                pointsOfPath[navMeshPath.corners.Length - 1] = playerPosition;
                for (int i = 0; i < navMeshPath.corners.Length; i++)
                {
                    pointsOfPath[i + 1] = navMeshPath.corners[i];
                }
                for (int i = 0; i < pointsOfPath.Length - 1; i++)
                {
                    pathLength += Vector3.Distance(pointsOfPath[i], pointsOfPath[i + 1]);
                }
            }
        }
        return pathLength;
    }


    private void ShootTarget()
    {
        enemyBody.StopMovement();
        enemyBody.ChangeLookAt(target.transform);
        transform.LookAt(Target.transform);

        Shoot();
    }


    private void GoToDestination(PlayerController target)
    {
        enemyBody.SetDestination(target.transform.position);
    }

    private void Shoot()
    {
        if (enemy.ActiveWeapon.GetComponent<RifleEnemyController>())
        {
            enemy.ShootRifle(Target);
        }
        if (enemy.ActiveWeapon.GetType().Equals(typeof(GunController)))
        {
            enemy.ShootGun(Target);
        }
    }

    //private void OnDrawGizmos()
    //{
    //    if (showGizmos)
    //    {
    //        Gizmos.color = Color.green;
    //        for (int i = 0; i < pathVectors.Length - 1; i++)
    //        {
    //            Gizmos.DrawRay(pathVectors[i], pathVectors[i + 1] - pathVectors[i]);
    //        }
    //    }
    //}
}
