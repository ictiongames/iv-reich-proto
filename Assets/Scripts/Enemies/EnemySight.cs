﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySight : MonoBehaviour {
    // Start is called before the first frame update
    private float fieldOfViewAngle;
    //private float eyeSightDistance;
    [SerializeField]
    private LayerMask layerMaskPlayer, layerMaskObjects;
    private PlayerController target, playerReference;

    [SerializeField]
    private Vector3 lastPlayerPosition;



    public PlayerController PlayerReference { get => playerReference; set => playerReference = value; }

    private EnemyController enemy;

    public float FieldOfViewAngle { get => fieldOfViewAngle; set => fieldOfViewAngle = value; }
    public Vector3 LastPlayerPosition { get => lastPlayerPosition; set => lastPlayerPosition = value; }

    private void Awake()
    {
        enemy = GetComponent<EnemyController>();
        fieldOfViewAngle = enemy.EnemyInfo.fieldOfViewAngle;
        //eyeSightDistance = enemy.EnemyInfo.maxEyeSightDistance;
    }
    public GameObject CheckIfSomeObjectEnterFieldOfView()
    {
        RaycastHit hit;
        Vector3 directionRight = Quaternion.AngleAxis(FieldOfViewAngle, transform.up) * transform.TransformDirection(Vector3.forward);
        Vector3 directionLeft = Quaternion.AngleAxis(-FieldOfViewAngle, transform.up) * transform.TransformDirection(Vector3.forward);

        if (Physics.Raycast(transform.position, directionRight, out hit, enemy.EnemyInfo.maxEyeSightDistance)
            || Physics.Raycast(transform.position, directionLeft, out hit, enemy.EnemyInfo.maxEyeSightDistance, layerMaskObjects.value))
        {
            return hit.collider.gameObject;
        }
        return null;
    }

    public bool CanSeePlayerFar()
    {
        bool canSee = false;

        Vector3 targetDir = playerReference.transform.position - transform.position;

        float angle = Vector3.Angle(targetDir, transform.forward);
        if (angle > -FieldOfViewAngle && angle < FieldOfViewAngle
            && Vector3.Distance(transform.position, PlayerReference.transform.position) < enemy.EnemyInfo.maxEyeSightDistance
            && Vector3.Distance(transform.position, PlayerReference.transform.position) > enemy.EnemyInfo.middleEyeSightDistance
            )
        {
            canSee = true;
        }

        return canSee;
    }

    public bool CanSeePlayerNear()
    {
        bool canSee = false;

        Vector3 targetDir = playerReference.transform.position - transform.position;

        float angle = Vector3.Angle(targetDir, transform.forward);
        if (angle > -FieldOfViewAngle && angle < FieldOfViewAngle
            && Vector3.Distance(transform.position, PlayerReference.transform.position) < enemy.EnemyInfo.middleEyeSightDistance
            && !this.CheckIfObjectsBetweenPlayerAndEnemy())
        {
            canSee = true;
        }

        return canSee;
    }


    private void OnDrawGizmos()
    {
        if (enemy && enemy.EnemyInfo)
        {

            Gizmos.color = Color.red;
            Vector3 direction = transform.TransformDirection(Vector3.forward) * enemy.EnemyInfo.maxEyeSightDistance;
            Gizmos.DrawRay(transform.position, direction);

            Gizmos.color = Color.yellow;
            Vector3 directionMiddle = transform.TransformDirection(Vector3.forward) * enemy.EnemyInfo.middleEyeSightDistance;
            Gizmos.DrawRay(transform.position, direction);

            //Gizmos.color = Color.yellow;

            //Vector3 direction90 = Quaternion.AngleAxis(90, transform.up) * transform.TransformDirection(Vector3.forward) * rayDistance;
            //Gizmos.DrawRay(transform.position, direction90);

            //Gizmos.color = Color.red;

            //Vector3 directionMinus90 = Quaternion.AngleAxis(-90, transform.up) * transform.TransformDirection(Vector3.forward) * rayDistance;
            //Gizmos.DrawRay(transform.position, directionMinus90);

            Gizmos.color = Color.blue;
            Vector3 directionRight = Quaternion.AngleAxis(FieldOfViewAngle, transform.up) * transform.TransformDirection(Vector3.forward) * enemy.EnemyInfo.maxEyeSightDistance;
            Gizmos.DrawRay(transform.position, directionRight);

            Gizmos.color = Color.blue;
            Vector3 directionLeft = Quaternion.AngleAxis(-FieldOfViewAngle, transform.up) * transform.TransformDirection(Vector3.forward) * enemy.EnemyInfo.maxEyeSightDistance;
            Gizmos.DrawRay(transform.position, directionLeft);
        }
    }

    private void Update()
    {
        if (LastPlayerPosition != Vector3.zero)
        {
            //Debug.Log(lastPlayerPosition);
        }
    }

    internal GameObject CheckIfObjectsBetweenPlayerAndEnemy()
    {
        Vector3 targetDir = playerReference.transform.position - transform.position;
        RaycastHit hit, hitPlayer;
        float distanceObstacle = 0.0f, distancePlayer = 0.0f;
        if (Physics.Raycast(transform.position, targetDir, out hit, enemy.EnemyInfo.maxEyeSightDistance, layerMaskObjects.value))
        {
            distanceObstacle = hit.distance;
        }
        if (Physics.Raycast(transform.position, targetDir, out hitPlayer, enemy.EnemyInfo.maxEyeSightDistance, layerMaskPlayer))
        {
            distancePlayer = hitPlayer.distance;
        }

        if (hit.collider && distanceObstacle < distancePlayer)
        {
            return hit.collider.gameObject;
        }
        return null;
    }
}
