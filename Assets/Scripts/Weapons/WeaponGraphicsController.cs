﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(ParticleSystem))]

public class WeaponGraphicsController : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem muzzleEffect;
    [SerializeField]
    private GameObject hitEffectPrefab;


    public GameObject HitEffectPrefab { get => hitEffectPrefab; set => hitEffectPrefab = value; }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    internal void InstantiateMuzzleEffect(Transform position)
    {
        GameObject muzzleEffectInstance = Instantiate(muzzleEffect.gameObject, position);
        muzzleEffectInstance.GetComponent<ParticleSystem>().Play();
    }
}
