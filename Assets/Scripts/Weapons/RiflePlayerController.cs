﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RiflePlayerController : RifleBase {

    Camera camera;
    [SerializeField]
    private Transform pointPosition;
    public Transform PointPosition { get => pointPosition; }

    private void Awake()
    {
        camera = GetComponentInParent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!IsInOrigin())
        {
            MoveToOrigin();
        }
    }


    public override void Shoot()
    {
        RaycastHit ray;
        WeaponGraphicsController.InstantiateMuzzleEffect(FirePosition);
        if (Physics.Raycast(camera.transform.position, camera.transform.forward, out ray))
        {
            EnemyController enemyController = ray.collider.gameObject.GetComponent<EnemyController>();
            if (enemyController)
            {
                Debug.Log("Enemy " + enemyController.gameObject.name);
                enemyController.ReceiveDamage(this.weapon.damage);

            }
            GameObject hitEffectPrefab = Instantiate(WeaponGraphicsController.HitEffectPrefab, ray.point, Quaternion.LookRotation(ray.normal));
            Destroy(hitEffectPrefab, 2.0f);
        }
        NumBullets--;
    }

    public override void Reload()
    {
        //numBullets = numBulletsMax;
        if (NumBullets == 0)
        {
            if (NumBulletLoaders > 0)
            {
                NumBullets = NumBulletAtLoader;
                NumBulletAtLoader = NumMaxBulletLoaders;
                NumBulletLoaders--;
            }
        }
        else
        {
            if (NumBulletAtLoader > 0)
            {
                NumBulletAtLoader = NumBulletsMax;
                //numBulletAtLoader = numBulletAtLoader - (numBulletsMax - numBullets);
                NumBullets = NumBulletsMax;
            }
            else
            {
                if (NumBulletLoaders > 0)
                {
                    NumBullets = NumBulletsMax;

                    NumBulletAtLoader = NumBulletsMax;
                    NumBulletLoaders--;
                }
            }
        }
    }
}
