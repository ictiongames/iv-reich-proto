﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RifleEnemyController : RifleBase {
    // Start is called before the first frame update
    private PlayerController target;
    private EnemyController enemy;

    //public PlayerController Target { get => target; set => target = value; }
    public EnemyController Enemy { get => enemy; set => enemy = value; }

    void Start()
    {
        this.Init();
        //Rifle = (Rifle)weapon;
        //NumBullets = Rifle.NumBalasCargador;
        //NumBulletsMax = Rifle.NumBalasCargador;
        Enemy = GetComponentInParent<EnemyController>();
        ////camera = GetComponentInParent<Camera>();
        //NumMaxBulletLoaders = 3;
        //NumBulletLoaders = NumMaxBulletLoaders;
        //NumBulletAtLoader = NumBulletsMax;
        //WeaponGraphicsController = GetComponent<WeaponGraphicsController>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void Shoot()
    {
        NumBullets--;
        RaycastHit ray;
        WeaponGraphicsController.InstantiateMuzzleEffect(FirePosition);
        if (Physics.Raycast(Enemy.transform.position, Enemy.transform.forward, out ray))
        {
            PlayerController playerController = ray.collider.gameObject.GetComponent<PlayerController>();
            if (playerController)
            {
                transform.LookAt(playerController.transform);
                Debug.Log("Player " + playerController.gameObject.name);
                playerController.AddDamage(this.weapon.damage);

            }
            GameObject hitEffectPrefab = Instantiate(WeaponGraphicsController.HitEffectPrefab, ray.point, Quaternion.LookRotation(ray.normal));
            Destroy(hitEffectPrefab, 2.0f);
        }
    }

    public override void Reload()
    {
        throw new System.NotImplementedException();
    }
}
