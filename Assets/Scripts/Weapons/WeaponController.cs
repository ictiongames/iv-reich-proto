﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class WeaponController : MonoBehaviour, IWeaponController {


    [SerializeField]
    protected Transform originalPosition;
    private Rigidbody body;

    //public Transform PointPosition { get => pointPosition; }
    public Transform OriginalPosition { get => originalPosition; set => originalPosition = value; }
    protected Rigidbody Body { get => body; set => body = value; }
    protected bool showGizmos;

    // Start is called before the first frame update
    void Awake()
    {
        Body = GetComponent<Rigidbody>();
        transform.localPosition = OriginalPosition.localPosition;
        transform.localRotation = OriginalPosition.localRotation;
    }


    public void MoveWeapon()
    {
        if (GetType().Equals(typeof(GranadeController)))
        {

        }
        else if (GetType().Equals(typeof(RifleBase)))
        {
            RifleBase fusil = (RifleBase)this;
            if (fusil)
            {
                fusil.Move();
            }
        }
        else if (GetType().Equals(typeof(GunController)))
        {
            GunController gun = (GunController)this;
            if (gun)
            {
                gun.Move();
            }
        }
    }



    public bool IsInOrigin()
    {
        return transform.localPosition == OriginalPosition.localPosition;
    }

    internal void ShowGizmos(bool v)
    {
        showGizmos = v;
    }

    public void MoveAway()
    {
        transform.localPosition = Vector3.Lerp(transform.localPosition, transform.localPosition - transform.forward * 1, 0.2f);
    }

    public void Activate(bool status)
    {
        showGizmos = status;
        foreach (MeshRenderer itemMesh in GetComponentsInChildren<MeshRenderer>(true))
        {
            itemMesh.gameObject.SetActive(status);
        }
        foreach (Collider collider in GetComponentsInChildren<Collider>(true))
        {
            collider.gameObject.SetActive(status);
        }
        foreach (ParticleSystem item in GetComponentsInChildren<ParticleSystem>(true))
        {
            item.gameObject.SetActive(status);
        }
    }

    public void MoveToOrigin()
    {
        transform.localPosition = Vector3.Lerp(transform.localPosition, OriginalPosition.localPosition, 0.05f);
    }
}

public abstract class WeaponController<T> : WeaponController where T : IWeaponController {
    [SerializeField]
    protected Weapon weapon;
    public Weapon Weapon { get => weapon; set => weapon = value; }
}