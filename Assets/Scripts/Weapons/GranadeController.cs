﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GranadeController : WeaponController<GranadeController> {
    [SerializeField]
    private GameObject arandela;
    // Start is called before the first frame update
    [SerializeField]
    private bool thrown;

    private Granade granade;

    public event Action<GranadeController> OnGranadeExplode = delegate{};
    public bool Thrown { get => thrown; set => thrown = value; }

    void Update()
    {
        if (!Thrown)
        {
            if (!IsInOrigin())
            {
                MoveToOrigin();
            }
        }
    }

    private void Start()
    {
        Body.isKinematic = true;
        granade = (Granade)weapon;
    }

    internal void ThrowGranade()
    {
        arandela.gameObject.SetActive(false);
        Vector3 vectorThow = transform.up * 45.0f + transform.forward * 45.0f;
        Body.useGravity = true;
        Body.isKinematic = false;
        Body.AddForce(vectorThow * 10.0f, ForceMode.Force);
        transform.parent = null;
        Thrown = true;
        Invoke("Explode", granade.ExplodeTime);
    }

    private void Explode()
    {
        //Explosion
        //Reset
        gameObject.SetActive(false);
        Thrown = false;
        Body.useGravity = false;
        OnGranadeExplode(this);
    }

    internal void ResePositionst()
    {
        transform.localPosition = OriginalPosition.localPosition;
        transform.localRotation = OriginalPosition.localRotation;
        transform.localScale =    OriginalPosition.localScale;
        Body.isKinematic = true;
    }
}
