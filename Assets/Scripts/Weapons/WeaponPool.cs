﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WeaponPool : MonoBehaviour {
    [SerializeField]
    private List<GameObject> weaponPool;
    [SerializeField]
    private List<GameObject> granadePool;
    [SerializeField]
    private List<GameObject> oneHandWeaponPool;

    [SerializeField]
    private int numGranades, lastOneHandWeaponSelected;
    [SerializeField]
    private List<GameObject> weaponInstances;

    [SerializeField]
    private List<GameObject> granadeInstances;

    [SerializeField]
    private List<GameObject> oneHandInstances;

    public List<GameObject> WeaponInstances { get => weaponInstances; set => weaponInstances = value; }
    public List<GameObject> GranadeInstances { get => granadeInstances; set => granadeInstances = value; }

    public List<GameObject> OneHandInstances { get => oneHandInstances; set => oneHandInstances = value; }
    public int LastOneHandWeaponSelected { get => lastOneHandWeaponSelected; set => lastOneHandWeaponSelected = value; }

    // Start is called before the first frame update
    void Awake()
    {
        foreach (var item in weaponPool)
        {
            GameObject weapon = Instantiate(item, transform, false);

            weapon.gameObject.SetActive(true);
            weaponInstances.Add(weapon);
        }
        for (int i = 0; i < numGranades; i++)
        {
            GameObject item = granadePool[0];
            GameObject granade = Instantiate(item, transform, false);


            granade.gameObject.SetActive(false);
            GranadeInstances.Add(granade);
        }
        foreach (var item in oneHandWeaponPool)
        {
            GameObject weapon = Instantiate(item, transform, false);

            weapon.gameObject.SetActive(true);
            oneHandInstances.Add(weapon);
        }
    }


    private void Start()
    {
        for (int i = WeaponInstances.Count - 1; i >= 1; i--)
        {
            GameObject item = WeaponInstances[i];
            item.GetComponent<WeaponController>().Activate(false);
            RifleBase rifle = item.GetComponent<RifleBase>();
            if (item.GetComponent<RifleBase>())
            {
                rifle.Init();
            }
        }
        for (int i = 0; i < GranadeInstances.Count; i++)
        {
            GameObject item = GranadeInstances[i];
            item.GetComponent<GranadeController>().Activate(false);
            item.GetComponent<GranadeController>().OnGranadeExplode += ResetGranade;
        }
        for (int i = 0; i < oneHandInstances.Count; i++)
        {
            GameObject item = OneHandInstances[i];
            item.GetComponent<OneHandWeaponController>().Activate(false);
        }
        LastOneHandWeaponSelected = 0;
    }

    private void OnDestroy()
    {
        if (GranadeInstances != null)
        {
            for (int i = 0; i < GranadeInstances.Count; i++)
            {
                GameObject item = GranadeInstances[i];
                item.GetComponent<GranadeController>().Activate(false);
                item.GetComponent<GranadeController>().OnGranadeExplode -= ResetGranade;
            }
        }
    }

    private void ResetGranade(GranadeController obj)
    {
        obj.transform.parent = transform;

        obj.ResePositionst();
        GranadeInstances.Add(obj.gameObject);
    }

    public GranadeController GetGranadeController()
    {
        if (GranadeInstances.Any(x => !x.GetComponent<GranadeController>().Thrown))
        {
            GranadeController granadeController = GranadeInstances.First(x => !x.GetComponent<GranadeController>().Thrown).GetComponent<GranadeController>();
            granadeController.Activate(true);
            granadeController.gameObject.SetActive(true);
            return granadeController;
        }
        else
        {
            return null;
        }
    }


    public WeaponController GetWeaponController(int i)
    {
        return weaponInstances[i].GetComponent<WeaponController>();
    }

    internal WeaponController GetOneHandWeaponController(int index)
    {
        if (index >= oneHandInstances.Count)
        {
            index = 0;
        }
        else if (index < 0)
        {
            index = oneHandInstances.Count - 1;
        }
        LastOneHandWeaponSelected = index;
        return oneHandInstances[index].GetComponent<WeaponController>();
    }
}
