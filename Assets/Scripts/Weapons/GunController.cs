﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GunController : WeaponController<IGunController>, IGunController {
    //bullets at current loader
    public int numBulletAtLoader;
    //Actual num loader used
    public int numBulletLoaders;
    //Num Max Bullets per Loader
    private int numBulletsMax;
    public int numMaxBulletLoaders;

    Camera camera;
    //private float cadenceBullets;
    private Gun gun;
    // Start is called before the first frame update
    private int numBullets;
    [SerializeField]
    private Transform pointPosition;
    [SerializeField]
    private Transform firePosition;
    [SerializeField]
    private GameObject timerPrefab;
    [SerializeField]
    private float range;
    //private List<GameObject> bulletPool = new List<GameObject>();
    private WeaponGraphicsController weaponGraphicsController;
    public int NumBulletAtLoader { get => numBulletAtLoader; set => numBulletAtLoader = value; }
    public int NumBullets { get => numBullets; set => numBullets = value; }
    public Transform PointPosition { get => pointPosition; }
    public int NumBulletsMax { get => numBulletsMax; set => numBulletsMax = value; }
    public Transform FirePosition { get => firePosition; set => firePosition = value; }
    public float Range { get => range; set => range = value; }
    public PlayerController Target { get => target; set => target = value; }

    private PlayerController target;

    public float GetCadenceBullets()
    {
        return gun.CadenceBullet;
    }


    public int GetUsableBullets()
    {
        //return numBullets - bulletPool.Count(x => x.activeInHierarchy);

        return numBullets;
    }

    public void Move()
    {
        float yValue = Mathf.Lerp(transform.localPosition.y - 0.005f, transform.localPosition.y + 0.005f, Mathf.PingPong(Time.time, 1));
        transform.localPosition = new Vector3(transform.localPosition.x, yValue, transform.localPosition.z);
    }

    public void Reload()
    {
        //numBullets = numBulletsMax;
        if (numBullets == 0)
        {
            if (numBulletLoaders > 0)
            {
                numBullets = numBulletAtLoader;
                numBulletAtLoader = numMaxBulletLoaders;
                numBulletLoaders--;
            }
        }
        else
        {
            if (numBulletAtLoader > 0)
            {
                if (numBulletAtLoader - (NumBulletsMax - numBullets) < 0)
                {
                    if (numBulletLoaders > 0)
                    {
                        numBullets = NumBulletsMax;

                        numBulletAtLoader = NumBulletsMax;
                        numBulletLoaders--;
                    }
                }
                else
                {
                    numBulletAtLoader = numBulletAtLoader - (NumBulletsMax - numBullets);

                    numBullets = NumBulletsMax;
                }
            }
            else
            {
                if (numBulletLoaders > 0)
                {
                    numBullets = NumBulletsMax;

                    numBulletAtLoader = NumBulletsMax;
                    numBulletLoaders--;
                }
            }
        }
    }


    public void Shoot()
    {
        numBullets--;
        RaycastHit ray;
        weaponGraphicsController.InstantiateMuzzleEffect(FirePosition);
        if (Physics.Raycast(camera.transform.position, camera.transform.forward, out ray))
        {
            BottomCollider bottomCollider = ray.collider.gameObject.GetComponent<BottomCollider>();
            EnemyController enemyController = ray.collider.gameObject.GetComponent<EnemyController>();
            if (enemyController)
            {
                Debug.Log("Enemy " + enemyController.gameObject.name);
                enemyController.ReceiveDamage(this.weapon.damage);

            }

            GameObject hitEffectPrefab = Instantiate(weaponGraphicsController.HitEffectPrefab, ray.point, Quaternion.LookRotation(ray.normal));
            Destroy(hitEffectPrefab, 2.0f);

            //hitEffectPrefab.transform.position = ray.point;


        }
    }

    void Awake()
    {
       
        gun = (Gun)base.Weapon;
        camera = GetComponentInParent<Camera>();

        NumBulletsMax = gun.NumBalasCargador;
        numBullets = NumBulletsMax;
        numMaxBulletLoaders = 3;
        numBulletLoaders = numMaxBulletLoaders;
        numBulletAtLoader = NumBulletsMax;

        weaponGraphicsController = GetComponent<WeaponGraphicsController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!IsInOrigin())
        {
            MoveToOrigin();
        }
    }
    private void OnDrawGizmos()
    {
        if (base.showGizmos)
        {
            Gizmos.color = Color.blue;
            Vector3 direction = FirePosition.TransformDirection(Vector3.forward) * Range;
            Gizmos.DrawRay(FirePosition.position, direction);
        }
    }

}
