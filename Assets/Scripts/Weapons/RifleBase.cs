﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(WeaponGraphicsController))]
public abstract class RifleBase : WeaponController<IRifleController>, IRifleController {
    // Start is called before the first frame update
    private Rifle rifle;
    private int numBullets;

    private int numBulletLoaders;
    private int numBulletAtLoader;
    private WeaponGraphicsController weaponGraphicsController;


    [SerializeField]
    private Transform firePosition;
    [SerializeField]
    private bool quickShoot;
    [SerializeField]
    private int numBulletsMax;
    [SerializeField]
    private int numMaxBulletLoaders;
    [SerializeField]
    private float range;


    public bool QuickShoot { get => quickShoot; }
    public int NumBullets { get => numBullets; set => numBullets = value; }
    protected int NumBulletAtLoader { get => numBulletAtLoader; set => numBulletAtLoader = value; }
    public int NumBulletsMax { get => numBulletsMax; set => numBulletsMax = value; }
    protected Transform FirePosition { get => firePosition; set => firePosition = value; }
    protected int NumMaxBulletLoaders { get => numMaxBulletLoaders; set => numMaxBulletLoaders = value; }
    protected int NumBulletLoaders { get => numBulletLoaders; set => numBulletLoaders = value; }
    protected Rifle Rifle { get => rifle; set => rifle = value; }
    protected WeaponGraphicsController WeaponGraphicsController { get => weaponGraphicsController; set => weaponGraphicsController = value; }
    public float Range { get => range; set => range = value; }

    public abstract void Shoot();
    public abstract void Reload();

   public void Init()
    {
        Rifle = (Rifle)weapon;
        NumBullets = Rifle.NumBalasCargador;
        NumBulletsMax = Rifle.NumBalasCargador;
        NumBulletLoaders = NumMaxBulletLoaders;
        NumBulletAtLoader = NumBulletsMax;

        WeaponGraphicsController = GetComponent<WeaponGraphicsController>();
    }

    public float GetCadenceBullets()
    {
        return Rifle.CadenceBullet;
    }


    public virtual void Move()
    {
        float yValue = Mathf.Lerp(transform.localPosition.y - 0.005f, transform.localPosition.y + 0.005f, Mathf.PingPong(Time.time, 1));
        transform.localPosition = new Vector3(transform.localPosition.x, yValue, transform.localPosition.z);
    }

    private void OnDrawGizmos()
    {
        if (base.showGizmos)
        {
            Gizmos.color = Color.blue;
            Vector3 direction = FirePosition.TransformDirection(Vector3.forward) * Range;
            Gizmos.DrawRay(FirePosition.position, direction);
        }
    }

}
