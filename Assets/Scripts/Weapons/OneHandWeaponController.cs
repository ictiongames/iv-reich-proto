﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneHandWeaponController : WeaponController {
    // Start is called before the first frame update
    private Animator animator;
    private bool hit;
    [SerializeField]
    private GameObject particlesPrefab;

    public bool Hit { get => hit; set => hit = value; }

    private void Start()
    {
        animator = GetComponent<Animator>();
    }
    internal void DoHit()
    {
        animator.SetTrigger("Hit");
        Hit = true;
    }

    internal void HitEnded()
    {
        Hit = false;

    }

    private void Update()
    {
        if (!IsInOrigin())
        {
            MoveToOrigin();
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (hit && collision.collider != null)
        {
            Debug.Log("Collision" + collision.collider); 
            GameObject hitEffectPrefab = Instantiate(particlesPrefab, collision.GetContact(0).point, Quaternion.LookRotation(collision.GetContact(0).normal));
            Destroy(hitEffectPrefab, 2.0f);

            //GameObject particles = Instantiate(particlesPrefab, collision.transform, false);
            //particles.transform.position = collision.GetContact(0).point
            //particles.transform.no
            //hit.
        }
    }
}
