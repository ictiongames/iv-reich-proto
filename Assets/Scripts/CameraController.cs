﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    private bool cursorVisible;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
    }

    internal void LerpPosition(Vector3 localPosition, float time)
    {
        transform.localPosition = Vector3.Lerp(transform.localPosition, localPosition, time);
    }
}
