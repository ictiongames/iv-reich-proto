﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

//Crear clase padre y el resto hijas con interfaces
public class ItemController : MonoBehaviour {
    [SerializeField]
    private GameObject infoWindow;

    [SerializeField]
    private Item item;

    [SerializeField]
    private GameObject infoItemPlaceholder, titleItemPlaceHolder;

    private TextMeshProUGUI infoItem, titleItem;

    public Item Item { get => item; set => item = value; }

    public event System.Action<ItemController> OnUseHealthItem = delegate { };
    public event System.Action OnPointerExitItem = delegate { };


    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Image>().sprite = Item.sprite;
        infoItem = infoItemPlaceholder.GetComponent<TextMeshProUGUI>();
        titleItem = titleItemPlaceHolder.GetComponent<TextMeshProUGUI>();

        infoItem.text = Item.information;
        titleItem.text = Item.itemName;

    }

    private void OnEnable()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnMouseClick()
    {
        Debug.Log("CLICK");
#pragma warning disable CC0120 // Your Switch maybe include default clause
        switch (Item.type)
        {
            case ItemType.HEALTH:
                OnUseHealthItem(this);

                break;
            case ItemType.ARMOR:
                break;
            case ItemType.AMMO:
                break;
            case ItemType.WEAPON:
                break;
        }
#pragma warning restore CC0120 // Your Switch maybe include default clause

    }

    public void OnPointerEnter()
    {
        //Debug.Log("Pointer Enter");
        infoWindow.gameObject.SetActive(true);
    }
    public void OnPointerExit()
    {
        //Debug.Log("Pointer Exit");
        infoWindow.gameObject.SetActive(false);
        OnPointerExitItem();
    }
}
