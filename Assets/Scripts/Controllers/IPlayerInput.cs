﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayerInput {

    bool Crotch();

    bool Jump();

    bool Point();

    float MoveCameraX();
    float MoveCameraY();

    float MovePlayerVertical();

    float MovePlayerHorizontal();


    bool OpenInventory();

    bool Reload();

    void ChangeWeapon();

    bool Action();

    bool ShootHoldOn();

    bool ShootOnce();

    bool SelectGranade();

    bool GetHandWeapon();

    float ChangeValueSelectedOneHandWeapon();
}
