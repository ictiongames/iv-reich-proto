﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardController : IPlayerInput {

    public float MovePlayerHorizontal()
    {
        return Input.GetAxis("Horizontal");
    }

    public float MovePlayerVertical()
    {
        return Input.GetAxis("Vertical");
    }
    public float MoveCameraX()
    {
        return Input.GetAxis("Mouse X");
    }

    public float MoveCameraY()
    {
        return Input.GetAxis("Mouse Y");
    }

    public bool Action()
    {
        return Input.GetKeyDown(KeyCode.E);
    }

    public void ChangeWeapon()
    {
        throw new NotImplementedException();
    }

    public void Crotch()
    {
        throw new NotImplementedException();
    }

    public bool SelectGranade()
    {
        return Input.GetKeyDown(KeyCode.G);
    }

    public bool GetHandWeapon()
    {
        return Input.GetKeyDown(KeyCode.Q);
    }

    public bool PlayerJump()
    {
        return Input.GetButtonDown("Jump");
    }

    public void Move()
    {
        throw new NotImplementedException();
    }

    public bool OpenInventory()
    {
        return Input.GetKeyDown(KeyCode.I);
    }

    public bool Point()
    {
        return Input.GetButton("Fire2");
    }

    public bool Reload()
    {
        return Input.GetKeyDown(KeyCode.R);
    }

    public void Shoot()
    {
        throw new NotImplementedException();
    }

    public bool ShootHoldOn()
    {
        return Input.GetButton("Fire1");
    }

    public bool ShootOnce()
    {
        return Input.GetButtonDown("Fire1");
    }

    bool IPlayerInput.Crotch()
    {
        throw new NotImplementedException();
    }

    bool IPlayerInput.Jump()
    {
        return Input.GetButtonDown("Jump");
    }

    public float ChangeValueSelectedOneHandWeapon()
    {
        return Input.mouseScrollDelta.y;
    }
}
