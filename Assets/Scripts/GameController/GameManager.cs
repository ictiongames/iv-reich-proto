﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Prefab(nameof(GameManager), true)]
public class GameManager : Singleton<GameManager> {


    [SerializeField]
    private bool gamePaused;

    [SerializeField]
    private PersistentData persistentData;



    [SerializeField]
    private bool vibration = false;
    public bool GamePaused { get => gamePaused; set => gamePaused = value; }
    public PersistentData PersistentData { get => persistentData; set => persistentData = value; }




    public bool Vibration { get => vibration; set => vibration = value; }

    public void Initialize()
    {
        if (PersistentData == null)
        {
            PersistentData = new PersistentData();
        }
        ReadInformation();
        //persistentData.Credits = 1000;
        //SaveInformation();
    }

    public void ReadInformation()
    {
        string data = PlayerPrefs.GetString("UserData");
        JsonUtility.FromJsonOverwrite(data, PersistentData);
    }
    public void SaveInformation()
    {
        string data = JsonUtility.ToJson(PersistentData);
        PlayerPrefs.SetString("UserData", data);
    }

    internal void PauseGame(bool value)
    {
        Time.timeScale = value ? 0 : 1;
        gamePaused = value;
    }

    private void GetPlatform()
    {

    }

}
