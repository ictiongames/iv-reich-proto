﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UIElements;

public interface IButton{
    System.Object GetButton();   
}


public enum BarItems { ITEMS, KEYITEMS, WEAPONS }
public class InventaryController : MonoBehaviour {
    [SerializeField]
    private GameObject panelItems, panelKeyItems, panelWeapons, crossPiece;
    [SerializeField]
    private GameObject healtemsPlaceHolder;
    [SerializeField]
    private List<GameObject> heallItems;
    [SerializeField]
    private Material material;
    public BarItems actualBarSelected;
    //private int actualItemSelected = 0;
    private CrossPieceController crossPieceController;
    private CanvasGroup canvasGroupCrossPiece;
    private bool assignMode = false, exitItem = false;
    private List<GameObject> healItemsInstances = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        actualBarSelected = BarItems.ITEMS;
        SetPanel();
        UnityEngine.Cursor.visible = true;
        crossPieceController = crossPiece.GetComponent<CrossPieceController>();
        canvasGroupCrossPiece = crossPiece.GetComponent<CanvasGroup>();

        foreach (GameObject item in heallItems)
        {
            GameObject itemInstance = Instantiate(item);
            healItemsInstances.Add(itemInstance);
            Material materialInstance = Instantiate(material);
            itemInstance.transform.parent = healtemsPlaceHolder.transform;
            GameObject text = new GameObject();
            TextMeshProUGUI textQuantity = text.AddComponent<TextMeshProUGUI>();
            textQuantity.text = "x2";
            textQuantity.fontSize = 25;
            text.transform.parent = itemInstance.transform;
            text.transform.localPosition = new Vector3(50, 30, 0); //Seteamos la posicion a mano para que este en la esquina supererior izq
            itemInstance.GetComponent<ItemController>().OnUseHealthItem += OnUseHealthItem;
            itemInstance.GetComponent<ItemController>().OnPointerExitItem += OnPointerExitItem;
        }
    }

    private void OnUseHealthItem(ItemController obj)
    {
        //Debug.Log("F");
        canvasGroupCrossPiece.alpha = 1.0f;
        canvasGroupCrossPiece.interactable = true;

        exitItem = false;
        StartCoroutine(WaitUntilValueToAssign((keyCode) =>
        {
            //Refactorizar para permitir cualquier control
            OnAddItemOnPosition(obj, keyCode);
        }));

    }

    private void OnAddItemOnPosition(ItemController obj, KeyCode keyCode)
    {
        switch (keyCode)
        {
            case KeyCode.A:
                crossPieceController.SetItem(obj.Item, CrossPosition.DOWN);
                break;
            case KeyCode.B:
                crossPieceController.SetItem(obj.Item, CrossPosition.LEFT);
                break;
            case KeyCode.C:
                crossPieceController.SetItem(obj.Item, CrossPosition.RIGHT);
                break;
            case KeyCode.D:
                crossPieceController.SetItem(obj.Item, CrossPosition.UP);
                break;
        }
    }

    private IEnumerator WaitUntilValueToAssign(System.Action<KeyCode> keyCode)
    {
        bool keypushed = false;

        while (!exitItem && !keypushed)
        {
            Debug.Log(Input.GetKeyDown(KeyCode.A));
            if (Input.GetKeyDown(KeyCode.A))
            {
                keypushed = true;
                keyCode(KeyCode.A);
            }
            if (Input.GetKeyDown(KeyCode.D))
            {
                keypushed = true;
                keyCode(KeyCode.D);

            }
            if (Input.GetKeyDown(KeyCode.B))
            {
                keypushed = true;
                keyCode(KeyCode.B);

            }
            if (Input.GetKeyDown(KeyCode.C))
            {
                keypushed = true;
                keyCode(KeyCode.B);

            }
            yield return null;
        }
    }

    private void OnPointerExitItem()
    {
        exitItem = true;
        //HideCrossPiece();
        canvasGroupCrossPiece.alpha = 0.5f;
        canvasGroupCrossPiece.interactable = false;
    }

    private void SetPanel()
    {
        DeactivatePanels();
        switch (actualBarSelected)
        {
            case BarItems.ITEMS:
                panelItems.gameObject.SetActive(true);

                break;
            case BarItems.KEYITEMS:
                panelKeyItems.gameObject.SetActive(true);

                break;
            case BarItems.WEAPONS:
                panelWeapons.gameObject.SetActive(true);

                break;
            default:
                throw new Exception("Unexpected Case");
        }
    }

    // Update is called once per frame
    void Update()
    {
        //if (assignMode)
        //{

        //    if (Input.GetKeyDown(KeyCode.A))
        //    {

        //    }
        //}
    }

    public void ItemBarClicked()
    {
        if (actualBarSelected != BarItems.ITEMS)
        {
            actualBarSelected = BarItems.ITEMS;
            SetPanel();
        }
        Debug.Log("Item");
    }

    private void DeactivatePanels()
    {
        panelItems.gameObject.SetActive(false);
        panelKeyItems.gameObject.SetActive(false);
        panelWeapons.gameObject.SetActive(false);
    }

    public void KeyItemsBarClicked()
    {
        if (actualBarSelected != BarItems.KEYITEMS)
        {
            actualBarSelected = BarItems.KEYITEMS;
            SetPanel();

        }
        Debug.Log("KeyITem");
    }

    public void WeaponsItemClicked()
    {
        if (actualBarSelected != BarItems.WEAPONS)
        {
            actualBarSelected = BarItems.WEAPONS;
            SetPanel();
        }
        Debug.Log("Weapons ITem");

    }
}
