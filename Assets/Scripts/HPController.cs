﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPController : MonoBehaviour {
    private RectTransform rectTransform;
    private Image image;
    private int initialLife;
    [SerializeField]
    private GameObject hPCointainer;
    public int InitialLife { get => initialLife; set => initialLife = value; }
    Color initialColor;
    // Start is called before the first frame update

    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        image = GetComponent<Image>();
        initialColor = image.color;


        
    }

    // Update is called once per frame
    void Update()
    {
        Color changedAlpha = image.color;
        changedAlpha.a = Mathf.Lerp(changedAlpha.a, 0.5f, 0.2f); ;
        image.color = changedAlpha;

    }

    internal void OnPlayerDamaged(int damage)
    {
        image.color = initialColor;
        rectTransform.localScale -= (Vector3.right * damage) / InitialLife;
    }

    internal void Hide(bool v)
    {
        hPCointainer.gameObject.SetActive(!v);
    }
}
