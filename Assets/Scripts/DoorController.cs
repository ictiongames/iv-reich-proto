﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class DoorController : MonoBehaviour {
    private Animator animator;

    [SerializeField]
    private CapsuleCollider _frontCollider, _backCollider;

    [SerializeField]
    private bool anyEnemyCanOpen;

    [SerializeField]
    private List<EnemyBody> allowedEnemies;

    private OffMeshLink offMeshLink;
    internal bool Open {
        get { return animator.GetBool("Open"); }
    }

    public event Action OnOpenDoor = delegate { };
    public event Action OnCloseDoor = delegate { };

    public bool AnyEnemyCanOpen { get => anyEnemyCanOpen; set => anyEnemyCanOpen = value; }
    public CapsuleCollider FrontCollider { get => _frontCollider; set => _frontCollider = value; }
    public CapsuleCollider BackCollider { get => _backCollider; set => _backCollider = value; }
    public OffMeshLink OffMeshLink { get => offMeshLink; set => offMeshLink = value; }
    public List<EnemyBody> AllowedEnemies { get => allowedEnemies; set => allowedEnemies = value; }

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponentInChildren<Animator>();
        offMeshLink = GetComponentInChildren<OffMeshLink>();
    }


    internal void OpenDoor()
    {
        animator.SetBool("Open", true);
        OnOpenDoor();
    }

    internal void CloseDoor()
    {
        animator.SetBool("Open", false);
        OnCloseDoor();

    }

    private void OnTriggerEnter(Collider other)
    {
        EnemyBody enemyBody = other.gameObject.GetComponent<EnemyBody>();
        if (enemyBody && CanOpenDoor(enemyBody))
        {
            if (!Open)
            {
                Debug.Log("Puerta se abre");
                OpenDoor();
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
      
        Gizmos.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) *5);

    }

    internal Vector3 GetEndPosition(Vector3 doorStart)
    {
        return Equals(doorStart, FrontCollider.transform.position) ? BackCollider.transform.position :  FrontCollider.transform.position;
    }

    public bool CanOpenDoor(EnemyBody enemyBody)
    {
        return !Open && (AnyEnemyCanOpen || AllowedEnemies.Contains(enemyBody));
    }
}
