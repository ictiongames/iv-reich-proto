﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CanvasController : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI textLife, textAmmo, actionText; 

    internal void SetAmmo(int ammo, int bulletsAtLoader)
    {
        string textAmmoOriginal = "Ammo {0} / {1}";
        textAmmo.text = String.Format(textAmmoOriginal, ammo, bulletsAtLoader);
    }

    internal void SetActionText(string text)
    {
        actionText.gameObject.SetActive(true);
        actionText.text = text;
    }

    internal void HideActionText()
    {
        actionText.gameObject.SetActive(false);
    }
}
