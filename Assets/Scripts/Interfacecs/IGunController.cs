﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGunController : IWeaponController
{
    void Shoot();
    int GetUsableBullets();
    float GetCadenceBullets();
    void Reload();
    void Move();

}
