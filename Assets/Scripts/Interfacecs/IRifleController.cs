﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRifleController : IWeaponController
{  
    float GetCadenceBullets();
    void Move();
    void Init();
}
