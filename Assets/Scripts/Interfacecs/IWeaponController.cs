﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IWeaponController {
    void MoveWeapon();
    bool IsInOrigin();
    void MoveAway();
    void Activate(bool status);

    void MoveToOrigin();
}
