﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CharactersController : MonoBehaviour{

    [SerializeField]
    private float speed, velocidadCorrer = 1, potenciaSalto, gravityForce;
    [SerializeField]
    private WeaponPool weaponPool;

    [SerializeField]
    private GameObject timerPrefab;

    private GameObject timerRifleInstance, timerGunInstance;
    private Timer timerGun;
    private Timer timer;

    public GameObject TimerPrefab { get => timerPrefab; set => timerPrefab = value; }
    public WeaponPool WeaponPool { get => weaponPool; set => weaponPool = value; }

    public Timer TimerGun { get => timerGun; set => timerGun = value; }
    public Timer TimerRifle { get => timer; set => timer = value; }

    public float Speed { get => speed; set => speed = value; }
    public float VelocidadCorrer { get => velocidadCorrer; set => velocidadCorrer = value; }
    public float PotenciaSalto { get => potenciaSalto; set => potenciaSalto = value; }
    public float GravityForce { get => gravityForce; set => gravityForce = value; }
    public GameObject TimerRifleInstance { get => timerRifleInstance; set => timerRifleInstance = value; }
    public GameObject TimerGunInstance { get => timerGunInstance; set => timerGunInstance = value; }
    public bool CanShotRifle { get => canShotRifle; set => canShotRifle = value; }
    public bool CanShotGun { get => canShotGun; set => canShotGun = value; }

    private bool canShotRifle = true, canShotGun = true;

    protected void CanShootGun()
    {
        CanShotGun = true;
    }

    protected void CanShootRifle()
    {
        CanShotRifle = true;
    }
}