﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottomCollider : MonoBehaviour
{
    //Used in LevelController
    public event Action<BulletController> OnBulletHitFloor = delegate { };

    private void OnTriggerEnter(Collider other)
    {
        BulletController bullet = other.GetComponent<BulletController>();
        if (bullet)
        {
            OnBulletHitFloor(bullet);
            Debug.Log("Enter Floor");
        }
    }
}
