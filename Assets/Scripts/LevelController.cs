﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LevelController : MonoBehaviour {
    [SerializeField]
    private List<BottomCollider> bottomColliders;

    //OBTENER MEDIANTE DI
    [SerializeField]
    private List<EnemyMind> enemyMinds;
    [SerializeField]
    private List<DoorController> doors;
    [SerializeField]
    private NavMeshSurface navMeshSurface;

    [SerializeField]
    private InventaryController inventary;

    private Canvas canvas;
    private HPController hpController;
    private CanvasController canvasController;
    private PlayerController playerController;

    private GameManager gameManager;
    // Start is called before the first frame update
    void Awake()
    {
        gameManager = GameManager.Instance;
        canvas = GameObject.FindObjectOfType<Canvas>();
        canvasController = canvas.GetComponent<CanvasController>();
        playerController = GameObject.FindObjectOfType<PlayerController>();
        hpController = canvas.GetComponentInChildren<HPController>();
        playerController.InitialLife = 100;

        hpController.InitialLife = playerController.InitialLife;

        PlayerController.OnChangeWeapon += OnChangeWeapon;
        PlayerController.OnPlayerShoot += OnPlayerShoot;
        PlayerController.OnPlayerMakeSound += PassSoundPositionToEnemies;
        PlayerController.OnPlayerHit += OnPlayerHit;
        PlayerController.OnPlayerDead += OnPlayerDead;
        PlayerController.OnPlayerHeal += OnPlayerHeal;
        PlayerController.OnReloadWeapon += OnPlayerOnReloadWeapon;
        PlayerController.OnFrontOfDoor += OnPlayerFrontOfDoor;
        PlayerController.OnPlayerIntereactWithDoor += OnPlayerIntereactWithDoor;
        PlayerController.OnHideActionText += OnHideActionText;
        PlayerController.OnOpenInventary += OnOpenInventary;


        for (int i = 0; i < enemyMinds.Count; i++)
        {
            EnemyMind item = enemyMinds[i];
            item.PlayerReference = playerController;
        }

        //foreach (var item in doors)
        //{
        //    item.OnOpenDoor += BakeNavMesh;
        //    item.OnCloseDoor += BakeNavMesh;
        //}
    }

    private void OnOpenInventary()
    {
        //Hide Life
        if (inventary.gameObject.activeInHierarchy)
        {
            inventary.gameObject.SetActive(false);
            GameManager.Instance.PauseGame(false);
            hpController.Hide(false);

        }
        else
        {
            inventary.gameObject.SetActive(true);
            hpController.Hide(true);
            GameManager.Instance.PauseGame(true);

        }
    }

    private void OnHideActionText()
    {
        canvasController.HideActionText();
    }

    private void OnPlayerIntereactWithDoor(DoorController door)
    {
        if (door.Open)
        {
            door.CloseDoor();
        }
        else
        {
            door.OpenDoor();
        }
    }

    private void OnPlayerFrontOfDoor(DoorController door)
    {
        if (door.Open)
        {
            canvasController.SetActionText("Press E to close Door");
        }
        else
        {
            canvasController.SetActionText("Press E to open Door");
        }
    }



    private void PassSoundPositionToEnemies(Vector3 positionToCheck)
    {
        foreach (EnemyMind enemyMind in enemyMinds.FindAll(x => x.Status != EnemyStatus.Dead && x.Status != EnemyStatus.RunAway))
        {
            enemyMind.CheckSoundAt(positionToCheck);
        }
    }

    private void OnPlayerOnReloadWeapon()
    {
        WeaponController activeWeapon = playerController.ActiveWeapon;
        SetAmmoOfWeapon(activeWeapon);
    }

    private void SetAmmoOfWeapon(WeaponController activeWeapon)
    {
        if (activeWeapon.GetType().Equals(typeof(GranadeController)))
        {

        }
        else if (activeWeapon.GetType().Equals(typeof(RiflePlayerController)))
        {
            RiflePlayerController rifle = (RiflePlayerController)activeWeapon;
            canvasController.SetAmmo(rifle.NumBullets, rifle.NumBulletsMax);
        }
        else if (activeWeapon.GetType().Equals(typeof(GunController)))
        {
            GunController gun = (GunController)activeWeapon;
            canvasController.SetAmmo(gun.NumBullets, gun.NumBulletsMax);
        }
    }

    private void OnPlayerHeal(int obj)
    {
        hpController.OnPlayerDamaged(-obj);
    }

    private void OnPlayerHit(int damageValue)
    {

        hpController.OnPlayerDamaged(damageValue);
    }
    private void OnPlayerDead()
    {
        Debug.Log("PlayerDead");
        //hpController.OnPlayerDamaged(damageValue);
    }

    private void OnEnemyHit(EnemyController enemyController)
    {
        Debug.Log("enemy Hit");
    }


    private void OnPlayerShoot(WeaponController obj)
    {
        OnChangeWeapon(obj);
    }


    private void OnDestroy()
    {
        if (playerController)
        {
            PlayerController.OnChangeWeapon -= OnChangeWeapon;
            PlayerController.OnPlayerShoot -= OnPlayerShoot;
            PlayerController.OnPlayerMakeSound -= PassSoundPositionToEnemies;
            PlayerController.OnPlayerHit -= OnPlayerHit;
            PlayerController.OnPlayerDead -= OnPlayerDead;
            PlayerController.OnPlayerHeal -= OnPlayerHeal;
            PlayerController.OnReloadWeapon -= OnPlayerOnReloadWeapon;
            PlayerController.OnFrontOfDoor -= OnPlayerFrontOfDoor;
            PlayerController.OnPlayerIntereactWithDoor -= OnPlayerIntereactWithDoor;
            PlayerController.OnHideActionText -= OnHideActionText;
            PlayerController.OnOpenInventary -= OnOpenInventary;

        }
        //if (bottomColliders != null)
        //{
        //    for (int i = 0; i < bottomColliders.Count; i++)
        //    {
        //        BottomCollider item = bottomColliders[i];
        //        item.OnBulletHitFloor -= OnBulletHitFloor;
        //    }
        //}
    }
    private void OnChangeWeapon(WeaponController obj)
    {
        SetAmmoOfWeapon(obj);
    }
}
