﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GranadeInstance", menuName = "Granade Instance")]
public class Granade : Weapon {

    [SerializeField]
    private float explodeTime;

    public float ExplodeTime { get => explodeTime; set => explodeTime = value; }
}
