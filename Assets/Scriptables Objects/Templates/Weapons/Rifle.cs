﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "RifleInstance", menuName = "Rifle Instance")]
public class Rifle : Weapon {
    [SerializeField]
    private int numBalasCargador;
    [SerializeField]
    private float cadenceBullet;
    public int NumBalasCargador { get => numBalasCargador; set => numBalasCargador = value; }
    public float CadenceBullet { get => cadenceBullet; set => cadenceBullet = value; }
}
