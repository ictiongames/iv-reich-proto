﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GunInstance", menuName = "Gun Instance")]
public class Gun : Weapon {
    [SerializeField]
    private int numBalasCargador;

    [SerializeField]
    private float cadenceBullet;
    public int NumBalasCargador { get => numBalasCargador; set => numBalasCargador = value; }
    public float CadenceBullet { get => cadenceBullet; set => cadenceBullet = value; }
}
