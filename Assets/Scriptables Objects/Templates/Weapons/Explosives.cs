﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ExplosiveInstance", menuName ="Explosives Intances")]
public class Explosives : Weapon {
    // Start is called before the first frame update
    [SerializeField]
    private int timeToExplode;
}
