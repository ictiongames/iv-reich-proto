﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WeaponInstance", menuName = "Weapons Instance")]
public class Weapon : ScriptableObject {
    [SerializeField]
    private int idWeapon;

    public string name;

    public int damage;
}
