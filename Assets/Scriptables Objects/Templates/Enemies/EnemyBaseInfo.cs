﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="EnemyBasic", menuName ="Enemies")]
public class EnemyBaseInfo : ScriptableObject
{
    public float health, maxEyeSightDistance, middleEyeSightDistance, distanceToShoot, hearRadius, timeToAlarm, lifeToRun, speed, speedRunning, fieldOfViewAngle, distanceToFollowPlayer;

    public int numMaxGranades = 1;
}
