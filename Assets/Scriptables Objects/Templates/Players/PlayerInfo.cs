﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="Player Info", menuName ="Player/StatsInfo")]
public class PlayerInfo : ScriptableObject
{

    public int numMaxGranades;
    public int  maxLife;
    public float SensitivityMouseX, SensitivityMouseY;
    public int potenciaSalto;
    public int speed;
    public int SpeedRunning;
    public int gravityForce;
}
