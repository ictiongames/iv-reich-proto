﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemType { HEALTH, ARMOR, AMMO, WEAPON }

[CreateAssetMenu(fileName = "ItemInstance", menuName ="Inventary Items")]
public class Item : ScriptableObject
{
    // Start is called before the first frame update
    public Sprite sprite;
    public int id;
    public string itemName;
    public string information;
    public ItemType type;
}
