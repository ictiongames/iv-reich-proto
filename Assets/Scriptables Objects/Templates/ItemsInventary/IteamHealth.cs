﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "HealthItem", menuName = "Items/HealthItem")]

public class IteamHealth : Item
{

    public float HealthRestoreValue;
    public IteamHealth()
    {
        type = ItemType.HEALTH;
    }
}
