﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum CrossPosition { LEFT, RIGHT, UP, DOWN }
public class CrossPieceController : MonoBehaviour {
    [SerializeField]
    private GameObject leftItem, rightItem, upItem, downItem;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void SetItem(Item item, CrossPosition position)
    {
        switch (position)
        {
            case CrossPosition.LEFT:
                leftItem.GetComponent<Image>().sprite = item.sprite;
                break;
            case CrossPosition.RIGHT:
                rightItem.GetComponent<Image>().sprite = item.sprite;

                break;
            case CrossPosition.UP:
                upItem.GetComponent<Image>().sprite = item.sprite;

                break;
            case CrossPosition.DOWN:
                downItem.GetComponent<Image>().sprite = item.sprite;

                break;
        }
    }
}
